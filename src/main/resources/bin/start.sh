#!/bin/sh
echo -------------------------------------------
echo start server
echo -------------------------------------------
# 项目路径
export CODE_HOME="/ops/deploy/elastic-job"
# 日志路径
export LOG_PATH="/ops/logs/elastic-job"
# 日志输出
export STDOUT_FILE="$LOG_PATH/stdout.log"
# 创建目录
mkdir -p $LOG_PATH

# JAVA路径
export JAVA_EXEC="$JAVA_HOME/bin/java"
# JVM启动参数
export JAVA_OPTS="-server -Xms512m -Xmx512m"

# 项目依赖路径
export CLASSPATH="$CODE_HOME/conf:$CODE_HOME/lib/*"
# 启动类
export MAIN_CLASS=com.elastic.main.SpringMain

$JAVA_EXEC $JAVA_OPTS -classpath $CLASSPATH $MAIN_CLASS > $STDOUT_FILE 2>&1 &

echo STDOUT:$STDOUT_FILE