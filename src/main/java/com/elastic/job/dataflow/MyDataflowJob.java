package com.elastic.job.dataflow;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @author anquan
 * desc: 把任务切分成n个分片,同一时刻有且仅有一个实例与分片对应.
 * date 2018-09-13 20:12
 */
public class MyDataflowJob implements DataflowJob<Integer> {

    Integer count = 0;

    /**
     * 获取数据(流式处理: fetchData不停的取数据直到返回null停止调用processData)
     * @param shardingContext
     * @return
     */
    @Override
    public List<Integer> fetchData(ShardingContext shardingContext) {
        System.out.println(String.format("-- fetchData ------ Time: %s, Thread ID: %s, 任务总片数: %s, 当前分片值: %s", new Date(),
                Thread.currentThread().getId(), shardingContext.getShardingTotalCount(), shardingContext.getShardingItem()));

        Integer item = shardingContext.getShardingItem();
        // 获取传递参数
        String param = shardingContext.getShardingParameter();

        if(count > 11){
            return null;
        }
        List<Integer> list = new ArrayList<>();
        switch (item) {
            case 0:
                list.add(count++);
                return list;
            case 1:
                list.add(count++);
                return list;
            case 2:
                list.add(count++);
                return list;
            case 3:
                list.add(count++);
                return list;
            case 4:
                list.add(count++);
                return list;
            case 5:
                list.add(count++);
                return list;
            case 6:
                list.add(count++);
                return list;
            case 7:
                list.add(count++);
                return list;
            case 8:
                list.add(count++);
                return list;
            case 9:
                list.add(count++);
                return list;
            default:
                return null;
        }
    }

    /**
     * 执行业务
     * @param shardingContext
     * @param list
     */
    @Override
    public void processData(ShardingContext shardingContext, List<Integer> list) {
        System.out.println(String.format("-- processData ------ Item: %s | Time: %s | Thread: %s | %s",
                shardingContext.getShardingItem(), new SimpleDateFormat("HH:mm:ss").format(new Date()), Thread.currentThread().getId(), "DATAFLOW PROCESS"));

        for (Integer each : list) {
            each++;
        }
    }
}
