package com.elastic.job.simple;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import java.util.Date;

/**
 * @author anquan
 * desc:
 * date 2018-09-13 14:08
 */
public class MySimpleJob implements SimpleJob {
    /**
     * 同一时刻,每一个分片有且仅有一个实例与之对应
     * @param shardingContext
     */
    @Override
    public void execute(ShardingContext shardingContext) {
        // 当前分片值
        int item = shardingContext.getShardingItem();
        // 自定义传参
        String param = shardingContext.getShardingParameter();

        System.out.println(String.format("------ Time: %s, Thread ID: %s, 任务总片数: %s, 当前分片值: %s, 传递参数: %s", new Date(),
                Thread.currentThread().getId(), shardingContext.getShardingTotalCount(), item, param));

        // 执行业务处理
        switch (item) {
            case 0:
                // do something by sharding item 0
                break;
            case 1:
                // do something by sharding item 1
                break;
            case 2:
                // do something by sharding item 2
                break;
            case 3:
                // do something by sharding item 2
                break;
            case 4:
                // do something by sharding item 0
                break;
            case 5:
                // do something by sharding item 1
                break;
            case 6:
                // do something by sharding item 2
                break;
            case 7:
                // do something by sharding item 2
                break;
            case 8:
                // do something by sharding item 0
                break;
            case 9:
                // do something by sharding item 1
                break;
            default:
                return;
        }
    }
}
