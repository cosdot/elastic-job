package com.elastic.main;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.dataflow.DataflowJobConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.elastic.job.dataflow.MyDataflowJob;
import com.elastic.job.simple.MySimpleJob;
/**
 * @author anquan
 * desc: 直接用JAVA方式执行
 * date 2018-09-13 14:30
 */
public class JavaMain {

    private static final String ZOOKEEPER_LIST = "192.168.0.241:2181,192.168.0.242:2181,192.168.0.243:2181";

    public static void main(String[] args) {
        // 获取注册中心
        CoordinatorRegistryCenter regCenter = createRegistryCenter();

        // 启动简单任务
        setSimpleJob(regCenter);

        // 启动流式任务
        //setDataflowJob(regCenter);
    }

    /**
     * 注册中心
     * @return
     */
    private static CoordinatorRegistryCenter createRegistryCenter() {
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration(ZOOKEEPER_LIST,"elastic-job"));
        regCenter.init();
        return regCenter;
    }

    /**
     * 执行简单任务
     * @param regCenter
     */
    private static void setSimpleJob(final CoordinatorRegistryCenter regCenter) {
        // 定时配置
        String jobName = "javaSimpleJob";
        String corn = "0/10 * * * * ?";
        Integer shardingCount = 3;
        String shardingParam = "0=A,1=B,2=C";
        // 定义作业核心配置
        JobCoreConfiguration coreConfig = JobCoreConfiguration.newBuilder(jobName, corn, shardingCount).shardingItemParameters(shardingParam).build();
        // 定义SIMPLE类型配置
        SimpleJobConfiguration simpleJobConfig = new SimpleJobConfiguration(coreConfig, MySimpleJob.class.getCanonicalName());
        // 定义Lite作业根配置
        LiteJobConfiguration liteJobConfiguration = LiteJobConfiguration.newBuilder(simpleJobConfig).build();
        // 启动任务
        new JobScheduler(regCenter, liteJobConfiguration).init();
    }

    /**
     * 执行简单任务
     * @param regCenter
     */
    private static void setDataflowJob(final CoordinatorRegistryCenter regCenter) {
        // 定时配置
        String jobName = "javaDataflowJob";
        String corn = "30 * * * * ?";
        Integer shardingCount = 3;
        String shardingParam = "0=A,1=B,2=C";
        // 定义作业核心配置
        JobCoreConfiguration coreConfig = JobCoreConfiguration.newBuilder(jobName, corn, shardingCount).shardingItemParameters(shardingParam).build();
        // 定义SIMPLE类型配置
        DataflowJobConfiguration dataflowJobConfig = new DataflowJobConfiguration(coreConfig, MyDataflowJob.class.getCanonicalName(),true);
        // 定义Lite作业根配置
        LiteJobConfiguration liteJobConfiguration = LiteJobConfiguration.newBuilder(dataflowJobConfig).build();
        // 启动任务
        new JobScheduler(regCenter, liteJobConfiguration).init();
    }
}
