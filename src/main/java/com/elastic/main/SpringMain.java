package com.elastic.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class SpringMain {
    
    public static void main(final String[] args) {
        new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
    }
}
