# elastic-job

#### 项目介绍

1. 分布式作业调度(需zookeeper注册中心)

#### 开源项目
1. 分布式作业调度： XXL-JOB
1. 分布式配置中心： XXL-CONF
1. 分布式爬虫： XXL-CRAWLER
1. API管理平台： XXL-API
1. Emoji表情编解码库： XXL-EMOJI
1. 代码生成平台： XXL-Code-Generator

